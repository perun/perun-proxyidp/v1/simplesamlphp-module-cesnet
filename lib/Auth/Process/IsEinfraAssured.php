<?php

declare(strict_types=1);

namespace SimpleSAML\Module\cesnet\Auth\Process;

use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Error\Exception;

/**
 * Class IsEinfraAssured.
 *
 * This class puts assurance timestamp as {prefix}-1y representing
 * user's einfra eligibility is not older than 1 year.
 */
class IsEinfraAssured extends ProcessingFilter
{
    public const LOGGER_PREFIX = 'cesnet:IsEinfraAssured - ';

    public const USER_ASSURANCE_ATTRS = 'userAssuranceAttrNames';

    public const USER_ELIGIBILITIES_ATTR = 'userEligibilitiesAttrName';

    public const SESSION_ELIGIBILITIES_ATTR = 'sessionEligibilitiesAttrName';

    public const ELIGIBILITIES_ATTR_KEY = 'eligibilitiesAttrKey';

    public const PREFIX = 'assurancePrefix';

    private $userEligibilitiesAttr = 'eligibilities';

    private $sessionEligibilitiesAttr = 'session_eligibilities';

    private $assuranceAttrs = ['eduPersonAssurance'];

    private $prefix = 'prefix';

    private const SUFFIX = '-1y';

    private $eligibilityKey = 'einfracz';

    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $config = Configuration::loadFromArray($config);

        if ($config === null) {
            throw new Exception(
                self::LOGGER_PREFIX . ' configuration is missing or invalid!'
            );
        }

        $this->assuranceAttrs = $config->getArray(self::USER_ASSURANCE_ATTRS, $this->assuranceAttrs);
        $this->userEligibilitiesAttr = $config->getString(
            self::USER_ELIGIBILITIES_ATTR,
            $this->userEligibilitiesAttr
        );
        $this->sessionEligibilitiesAttr = $config->getString(
            self::SESSION_ELIGIBILITIES_ATTR,
            $this->sessionEligibilitiesAttr
        );
        $this->prefix = $config->getString(self::PREFIX, $this->prefix);
        $this->eligibilityKey = $config->getString(self::ELIGIBILITIES_ATTR_KEY, $this->eligibilityKey);

        if (empty($this->assuranceAttrs)) {
            throw new Exception(
                self::LOGGER_PREFIX . ' empty array detected !'
            );
        }
    }

    public function process(&$request)
    {
        $timestamp = 0;
        if (!empty($request['Attributes'][$this->sessionEligibilitiesAttr][$this->eligibilityKey])) {
            $timestamp = $request['Attributes'][$this->sessionEligibilitiesAttr][$this->eligibilityKey];
        } elseif (!empty($request['Attributes'][$this->userEligibilitiesAttr][$this->eligibilityKey])) {
            $timestamp = $request['Attributes'][$this->userEligibilitiesAttr][$this->eligibilityKey];
        }

        if ($timestamp > strtotime('-1 year')) {
            foreach ($this->assuranceAttrs as $attr) {
                $request['Attributes'][$attr][] = $this->prefix . self::SUFFIX;
            }
            Logger::debug(
                self::LOGGER_PREFIX .
                'Added assurance timestamp ' . $this->prefix . self::SUFFIX
            );
        } else {
            Logger::debug(
                self::LOGGER_PREFIX .
                'User is not assured - assurance timestamp not added.'
            );
        }
    }
}
