# einfra-aai-proxy-idp-template

Template for CESNET eInfra Proxy IdP component

## Contribution

This repository uses [Conventional Commits](https://www.npmjs.com/package/@commitlint/config-conventional).

Any change that significantly changes behavior in a backward-incompatible way or requires a configuration change must be marked as BREAKING CHANGE.

### Available scopes:

- theme
- Auth Process filters:
  - computeloa
  - iscesneteligible
  - iseinfraczeligible
  - iseinfraassured

## Instalation

`php composer.phar require cesnet/simplesamlphp-module-cesnet`

